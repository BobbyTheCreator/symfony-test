calculatePrice = (function ($) {

    let config = {
        calculateBtn: ".js-calculate-btn",
        priceWithTax: ".js-price-with-tax",
        calculatePriceForm: ".js-form",
        priceField: ".js-price-field"
    };

    $(function () {
        init();
    });

    let init = function () {
        setup();
    };

    let setup = function () {
        $(config.priceField).prop("type", "text");
        $(document).on("click", config.calculateBtn, calculatePrice);
    };

    let calculatePrice = function (e) {
        e.preventDefault();

        $.ajax({
            url: "/calculate-price",
            data: $(config.calculatePriceForm).serializeArray(),
            type: "POST",
            dataType: "json",
            async: true,
            error: function (xhr, status, error) {
                let parser = new DOMParser();
                let htmlObject = parser.parseFromString(xhr['responseText'], "text/html");
                $(config.calculatePriceForm).replaceWith(htmlObject.getElementsByTagName('form')[0]);
                $(config.priceField).prop("type", "text");
            }
        }).done(function (data) {
            let parser = new DOMParser();
            let htmlObject = parser.parseFromString(data['htmlResponse'], "text/html");
            $(config.calculatePriceForm).replaceWith(htmlObject.getElementsByTagName('form')[0]);
            $(config.priceField).prop("type", "text");

            $(config.priceWithTax).text('Price with tax = ' + data['priceWithTax']);
        });
    };

    return {
        init: init
    };
})(jQuery);
