<?php

namespace App\Controller;

use App\Entity\Country;
use App\Form\Type\CalculatePriceType;
use App\Form\Type\CountryType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class IndexController extends AbstractController
{
    #[Route('/', name: 'index')]
    public function index(): Response
    {
        return $this->render('index/index.html.twig', [
            'controller_name' => 'IndexController',
        ]);
    }

    #[Route('/create-country', name: 'create_country')]
    public function createCountry(Request $request): Response
    {
        // creates a task object and initializes some data for this example
        $country = new Country();

        $form = $this->createForm(CountryType::class, $country);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $country = $form->getData();

            $em = $this->getDoctrine()->getManager();
            $em->persist($country);
            $em->flush();

            return $this->redirect('/');
        }

        return $this->render('index/create-country.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    #[Route('/calculate-price', name: 'calculate-price')]
    public function calculatePrice(Request $request): Response
    {
        $form = $this->createForm(CalculatePriceType::class);
        $form->handleRequest($request);

        if ($request->isXmlHttpRequest() && $form->isValid()) {
            $data = $form->getData();

            $priceWithTax = ($data['price'] - $data['price'] / 100 * $data['country']->getTax());

            return new JsonResponse(
                [
                    'htmlResponse' => $this->renderView(
                        'index/calculate-price.html.twig',
                        [
                            'form' => $form->createView(),
                        ]
                    ),
                    'priceWithTax' => $priceWithTax ?? null,
                ]
            );
        }

        return $this->render('index/calculate-price.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
