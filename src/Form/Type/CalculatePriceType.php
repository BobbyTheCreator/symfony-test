<?php

declare(strict_types=1);

namespace App\Form\Type;

use App\Entity\Country;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\NotNull;

class CalculatePriceType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->add(
                'price',
                IntegerType::class,
                [
                    'constraints' => [
                        new NotNull()
                    ],
                ]
            )
            ->add(
                'country',
                EntityType::class,
                [
                    'constraints' => array(
                        new NotNull(),
                    ),
                    'class' => Country::class,
                    'choice_label' => 'name',
            ])
            ->add('save', SubmitType::class, ['label' => 'Calculate']);
    }
}